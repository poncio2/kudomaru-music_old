<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name'
  ];

  /**
   *
   * Gets the tracks associated to that particular genre.
   *
   */
  public function tracks()
  {
      return $this->hasMany('App\Track');
  }


}
